//    GWiS: a gene-based test of association
//    Copyright (C) 2011  Hailiang Huang, Pritam Chanda, Joel S. Bader and Dan E. Arking
//    Contact: joel.bader@jhu.edu
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License along
//    with this program; if not, write to the Free Software Foundation, Inc.,
//    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <pthread.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_statistics_double.h>
#include <gsl/gsl_fit.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_interp.h>
#include <gsl/gsl_sort_vector.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_linalg.h>


#define VERBOSE false
#define CQ_DEBUG false
#define DEBUG false

//minimum model size.  set to 0 in most cases
#define K_MIN 0
#define NO_SIGN_FLIP false
#define BOUND_BETA false

#define PI 3.1415926535
#define EPS 0.0000001
#define P0 0
//the corresponding R (not R2) to VIF= 5.  Set to prevent colinearity  
#define VIF_R 0.89442719
#define VIF_R2 0.8
//#define VIF_R 0.547722
//#define VIF_R2 0.3
//set N_HAT (number of heterozygous) and R2_CUTOFF to ensure the SNP quality.
#define N_HAT_CUTOFF 5
#define R2_CUTOFF 0.3

#define MAX_FILENAME_LEN 100
#define MAX_INCLUDED_SNP 20 
#define MAX_N_INDIV 20000
#define MAX_N_INDIV_NO_PHENO 2000
#define MAX_LINE_WIDTH 120000
//it is safe to estimate a smaller number.  The program will auto resize when necessary
#define MAX_SNP_PER_GENE 256
#define MAX_SNP_PER_GENE_FIXED 5000
#define MAX_COR_SNP 1024

#define PHENOTYPE_VALUE_LEN 20
#define INDIV_ID_LEN 20
#define SNP_ID_LEN 20
#define GENE_ID_LEN 2000

#define N_PERM_MIN 20
#define N_CUTOFF 10

#define min(x,y) ((x)<(y)?(x):(y))
#define max(x,y) ((x)>(y)?(x):(y))

#define SIGMA_A 0.2

bool SKIPPERM;
bool FULLSEARCH;
int N_PERM;
int FLANK;

bool INTERCEPT;
bool GET_SNP_PVAL;
bool GET_SNP_PERM_PVAL;
bool GET_BF_PVAL;
bool GET_VEGAS_PVAL;
bool GET_GENE_PVAL;
char* DATASET;

//int covariance_counter=0;
//int shuffle_counter = 0;

typedef struct SNP_TYPE 
{
  char name[SNP_ID_LEN];
  int gene_id;
  int chr;
  double pos;
  double MAF;
  double R2;
  int bp;
  int nGene;
  double f_stat;
  double BF;

  double sum_pheno_geno; //vector product (dot product) of the phenotype and genotype vectors for the SNP
  int nHit; //for minSNP, number of hits for the SNP, no need to permuate again in case of overlapping genes
  int iPerm; //for minSNP, number of perms for the SNP, no need to permuate again in case of overlapping genes
  int nHit_bonf; //for minSNP-P, number of hits for the SNP,  no need to permuate again in case of overlapping genes
  int iPerm_bonf;//for minSNP-P, number of perms for the SNP, no need to permuate again in case of overlapping genes
  double geno_tss; //tss of geno
  double *geno; //genotype
  double eSampleSize; //effective number of sample size, (imputation quality)*MAF*nsample

  double *r; //correlation with other SNPs
  char* r_names; 
  int n_correlated_snp;
  int n_correlated_snp_max;
} SNP;

typedef struct BIC_STATE_TYPE // data for GWiS
{
  double BIC[MAX_INCLUDED_SNP+1]; // GWiS test statistics for each SNP added to the model
  double RSS[MAX_INCLUDED_SNP+1]; // Redisual sum of squares for each SNP
  SNP* bestSNP[MAX_INCLUDED_SNP+1]; // best SNPs
  int iSNP; //model size, "k"
}BIC_STATE;

typedef struct HIT_COUNTER_TYPE // data structure to save how many permutations / hits we have done 
{
  int k_pick[MAX_INCLUDED_SNP+1];
  int k_hits[MAX_INCLUDED_SNP+1];
  int maxK;
  int hit_bic;
  int perm_bic;

  int hit_bf;
  int perm_bf;

  int hit_vegas;
  int perm_vegas;

  int * hit_snp;
  int * perm_snp;

  int * hit_snp_bonf;
  int * perm_snp_bonf;


}HIT_COUNTER;

typedef struct GENE_TYPE 
{
  char name[GENE_ID_LEN];
  char ccds[GENE_ID_LEN];
  int chr;
  int bp_start;
  int bp_end;
  int iPerm; // number of perms for GWiS

  int nSNP; // total number of SNPs in a gene
  double eSNP; // effective number of SNPs in a gene

  int snp_start; //coordinate for the first SNP in this gene in the circular queue
  int snp_end; //coordinate for the last SNP in this gene in the circular queue
  
  //fields for tests
  BIC_STATE bic_state; // GWiS test statistics, RSS, and SNPs chosen   
  HIT_COUNTER hits; // GWiS permutation results
  double BF_sum;
  double vegas;
  SNP* maxSSM_SNP; // for minSNP, best SNP picked up by minSNP, for this gene
  SNP* maxBonf_SNP;// for minSNP-P, best SNP picked up by minSNP-P, for this gene

  //fields for performance
  gsl_matrix* LD; // Correlation matrix

  gsl_matrix* Cov; // covariance matrix

  bool skip; // a indicator to skip this gene
  //fields for linked list

  struct GENE_TYPE * next;

} GENE;

typedef struct ORTHNORM_TYPE
{
  double norm; //norm of the genotype, will be adjusted in the ortho-nornmalization procedures
  double norm_original; //norm of the genotype, stays the same.  Compare norm and norm_original to look into VIF
  double projP; // projection of trait vector to the genotype vector, adjusted in each of the ortho-nornmalization procedures 
  int k; // step of the ortho-nornmalization procedures
  SNP * snp; // SNP corresponding to this data sructure
  double sum_X_bestZ[MAX_SNP_PER_GENE_FIXED]; //intermediate calculations, saved for future ortho-nornmalization procedures
}OrthNorm;

typedef struct C_QUEUE_TYPE 
{
  void * dat; // where the data is saved
  void * last; // pointer for the memory boundary
  size_t nsize; //size of each variable
  size_t chunkSize;
  size_t start; //start of the logic position
  size_t end; // end of the logic position

} C_QUEUE;

typedef struct BIC_THREAD_DATA_TYPE
{
  gsl_vector * pheno;
  gsl_rng *r;
  GENE* gene;
  C_QUEUE * snp_queue;
  double pheno_mean;
  double pheno_tss_per_n;
  int nsample;
  FILE* fp_SNP_diag; 
  
}BIC_THREAD_DATA;

typedef struct PHENOTYPE_TYPE //phenotype vector
{
  gsl_vector * pheno_array; //phenotype array
  double mean; //mean of the phenotype
  double tss_per_n; // tss/n of the phenotype
  int N_na; // number of NAs in the phenotype file
  int N_indiv; // number of individuals, including NA
  int N_sample; //number of samples, nsample=N_indiv - N_na
  bool NA[MAX_N_INDIV]; // which indiv we are missing
} PHENOTYPE;

C_QUEUE* cq_init(size_t nsize, size_t chunkSize){

  C_QUEUE* c_queue = (C_QUEUE*) malloc(sizeof(C_QUEUE));

  c_queue->dat = malloc(chunkSize * nsize);
  c_queue->last = c_queue->dat + chunkSize * (nsize-1);
  c_queue->nsize=nsize;
  c_queue->chunkSize=chunkSize;
  c_queue->start = 0;
  c_queue->end = 0;
  
  return c_queue;
}

bool cq_isInQ(size_t curItem, C_QUEUE* c_queue){
  
  bool ret = false;
  
  if(curItem >= c_queue->start && curItem < c_queue->end)
    ret = true;
  
  return ret;
}


void* cq_getItem(size_t i, C_QUEUE* c_queue){
  
  size_t pos  = i%c_queue->nsize * c_queue->chunkSize;
  return c_queue->dat+pos;

}

void* cq_getNext(void* ptr, C_QUEUE* c_queue){
  
  if(ptr != c_queue->last)
    ptr +=  c_queue->chunkSize;
  else
    ptr = c_queue->dat;

  return ptr;

}

bool cq_isEmpty(C_QUEUE* c_queue){

  if(c_queue->start == c_queue->end )
    return true;
  else
    return false;
}

bool cq_isFull(C_QUEUE* c_queue){

  if( c_queue->end - c_queue->start >= c_queue->nsize)
    return true;

  return false;
}

void cq_resize(C_QUEUE * c_queue){

  size_t newSize = 2* c_queue->nsize;
  void * newDat = calloc(newSize, c_queue->chunkSize);
  if(newDat==NULL){
    printf("-Failed to allocate %lux%lu=%lu memory.\n",c_queue->chunkSize,newSize, c_queue->chunkSize*newSize);
    abort();
  }else{
    printf("-Memory usage increased to %g M\n",(double)c_queue->chunkSize*newSize/1024/1024 );
  }

  size_t startTrans = c_queue->start%c_queue->nsize;
  size_t endTrans = c_queue->end%c_queue->nsize;
  size_t len = c_queue->end - c_queue->start;

  if(len > 0){
    if(endTrans > startTrans) 
      memcpy(newDat+c_queue->start%newSize*c_queue->chunkSize, cq_getItem(c_queue->start, c_queue), len*c_queue->chunkSize);
    else if(endTrans <= startTrans){
      memcpy(newDat+c_queue->start%newSize*c_queue->chunkSize, cq_getItem(c_queue->start, c_queue), (c_queue->nsize - c_queue->start%c_queue->nsize)*c_queue->chunkSize);
      memcpy(newDat+c_queue->end/c_queue->nsize%2*c_queue->nsize*c_queue->chunkSize, c_queue->dat, (c_queue->end%c_queue->nsize + 1)*c_queue->chunkSize);
    }
   
  }else if(len < 0){
    printf("The queue length is negative!\n");
    abort();
  }

  free(c_queue->dat);

  c_queue->dat = newDat;
  c_queue->nsize = newSize;
  c_queue->last = newDat + c_queue->chunkSize*(newSize-1);
  
}


void* cq_push(C_QUEUE* c_queue){

  void * ret;

  if(cq_isFull(c_queue)){
    printf("-Resize the queue from %lu to %lu\n", c_queue->nsize, c_queue->nsize*2);
    cq_resize(c_queue);
  }  
  ret = cq_getItem(c_queue->end, c_queue);
  
  c_queue->end ++;

  return ret;
}

//function to get one element from end 
void* cq_shift(C_QUEUE* c_queue){

  if(cq_isEmpty(c_queue)){
    printf("Queue is empty.  Can't shift\n");
    abort();
  }

  c_queue->end--;
 
  return cq_getItem(c_queue->end, c_queue);
}

//function to get one element from start 
void* cq_pop(C_QUEUE* c_queue, bool remove){

  void * ret;

  if(cq_isEmpty(c_queue)){
    printf("Queue is empty.  Can't pop\n");
    abort();
  }

  ret = cq_getItem(c_queue->start, c_queue);

  if(remove)
    c_queue->start ++;

  return ret;
}



char* getTime(){
  time_t raw_time;
  
  time(&raw_time);
  
  return ctime(&raw_time);
}

void getFilename(char* filename, char* dir, char* name, char* fix){
  strcat(filename, dir);
  strcat(filename, name);
  strcat(filename, fix);
}

gsl_rng* initRand(int seed){
	
  const gsl_rng_type * T;
  gsl_rng * r;
  gsl_rng_env_setup();
	
  T = gsl_rng_default;
  r = gsl_rng_alloc (T);
	
  gsl_rng_set(r, seed);
	
  return r;
}

//vector operations

void shuffle(gsl_vector * array, int nSize, gsl_rng *r, FILE* fp, char* sep){
	
  static int i, j;
  //  shuffle_counter ++;
  for(i=nSize; i>1; i--){
    j = gsl_rng_uniform_int(r, i);
    if(j != i ){
      gsl_vector_swap_elements(array, i-1, j);
    }
  }
  
  /*
    if(fp != NULL){
    for(i=0; i<nSize; i++)
    fprintf(fp, "%g%s", gsl_vector_get(array,i), sep);
    fprintf(fp, "\n");
    }*/
}

void zeroArray_int(int* a, int nsample){
  static int i;
  for(i=0; i<nsample; i++)
    a[i]=0;
}

double mean(double *v, int n){
  
  static int i;
  static double sum;
  sum=0;
  for(i=0; i<n; i++) sum+=v[i];
  return sum/n;

}


double tss(double* v, int n){
  
  static int i;
  static double sum;
  sum=0;
  double m = mean(v, n);
  for(i=0; i<n; i++) sum+= v[i]*v[i];
  if(INTERCEPT){
    return sum-n*m*m;
  }else{
    return sum;
  }
}


double variance(double* v, int n){
  
  static int i;
  static double sum;
  sum=0;
  double m = mean(v, n);
  for(i=0; i<n; i++) sum+= v[i]*v[i];
  if(INTERCEPT){
    return sum/n-m*m;
  }else{
    return sum/n;
  }
}

double covariance(double* v1, double* v2, int n){
  
  static int i;
  static double sum;
  static double s1;
  static double s2;
  
  //  covariance_counter++;
  sum=0;
  s1=0;
  s2 =0;
  for(i=0; i<n; i++){
    sum += v1[i]*v2[i];
    s1 +=v1[i];
    s2 +=v2[i];
  }
  if(INTERCEPT){
    return sum/n - s1/n*s2/n;
  }else{
    return sum/n;
  }
}

double correlation(double* v1, double* v2, int n){

  static int i;
  static double sum12;
  static double sum1;
  static double sum2;
  static double sum22;
  static double sum11;
  
  //  covariance_counter++;
  sum12=0;
  sum1=0;
  sum2 =0;
  sum11=0;
  sum22=0;

  for(i=0; i<n; i++){
    sum12 += v1[i]*v2[i];
    sum1 +=v1[i];
    sum2 +=v2[i];
    sum11 +=v1[i]*v1[i];
    sum22 +=v2[i]*v2[i];
  }

  return (sum12/n - sum1/n*sum2/n)/sqrt((sum11/n-sum1/n*sum1/n)*(sum22/n-sum2/n*sum2/n));
}

void scalePheno(PHENOTYPE* pheno){

  int i;

  for (i = 0; i < pheno->N_sample; i++){
    gsl_vector_set(pheno->pheno_array, i,  gsl_vector_get(pheno->pheno_array, i)/sqrt(pheno->tss_per_n));
  }

  pheno->mean=mean(pheno->pheno_array->data, pheno->N_sample );
  pheno->tss_per_n=variance(pheno->pheno_array->data, pheno->N_sample );
 
  printf("-after scaling, phenotype mean:%g, TSS per individual: %g\n",  pheno->mean, pheno->tss_per_n);
  
  //  fclose(fp);
}

void standardizePheno(PHENOTYPE* pheno){
  //rank the phenotypes and give the corresponding normal quantile to each individual
  gsl_permutation * perm = gsl_permutation_alloc(pheno->N_sample);
  gsl_permutation * rank = gsl_permutation_alloc(pheno->N_sample);

  gsl_sort_vector_index (perm, pheno->pheno_array);
  gsl_permutation_inverse (rank, perm);

  int i;

  for (i = 0; i < pheno->N_sample; i++){
    double q = gsl_cdf_ugaussian_Qinv(((double)rank->data[i]+0.5)/(pheno->N_sample));
    gsl_vector_set(pheno->pheno_array, i, q);
  }

  pheno->mean=mean(pheno->pheno_array->data, pheno->N_sample );
  pheno->tss_per_n=variance(pheno->pheno_array->data, pheno->N_sample );
 
  printf("%g, %g\n",  pheno->mean, pheno->tss_per_n);
  
  gsl_permutation_free (perm);
  gsl_permutation_free (rank);
}

double Proj_X_Z(OrthNorm* Z, OrthNorm** bestZ, gsl_matrix *Cov){

  double ret = 0;
  double cor=  gsl_matrix_get(Cov, Z->snp->gene_id, bestZ[Z->k-2]->snp->gene_id);
  int k;

  for(k=2; k<Z->k; k++)
    ret += Z->sum_X_bestZ[k-2]*bestZ[Z->k-2]->sum_X_bestZ[k-2]/bestZ[k-2]->norm;
  if(NO_SIGN_FLIP && ((cor-ret)*cor < 0 || fabs((cor-ret)*cor)<EPS)){
    return 0;
  }else{
    return cor-ret;
  }
}

double getNormZ(OrthNorm * Z, OrthNorm** bestZ){

  if(pow(Z->sum_X_bestZ[Z->k-2], 2)/ bestZ[Z->k-2]->norm <0){
    printf("allert: %s, %g, %g\n", Z->snp->name, Z->sum_X_bestZ[Z->k-2],  bestZ[Z->k-2]->norm);
  }
  return Z->norm - pow(Z->sum_X_bestZ[Z->k-2], 2)/ bestZ[Z->k-2]->norm;
  
}


double Proj_P_Z(OrthNorm* Z, OrthNorm**  bestZ){

  return Z->projP - bestZ[Z->k-2]->projP*Z->sum_X_bestZ[Z->k-2]/bestZ[Z->k-2]->norm;

}

void calculateZ(OrthNorm* Z, OrthNorm** bestZ,  int k,  gsl_matrix* Cov){

  Z->k = k;
  static double projP =0;
  static double norm =0;
  //k=1, 2, ....
  //start at k=2
  //k=1 skipped
  
  if(k>1){
    Z->sum_X_bestZ[k-2] = Proj_X_Z(Z, bestZ, Cov);
    norm = getNormZ(Z, bestZ);
    projP= Proj_P_Z(Z, bestZ);

    if(NO_SIGN_FLIP && (projP * Z->projP <0 || fabs(projP * Z->projP)<EPS )){
      Z->projP=0;
    }else{
      if(BOUND_BETA && projP / Z->projP >= norm/Z->norm ){
	projP = Z->projP* (norm / Z->norm);
      }
      Z->projP=projP;
    }

    Z->norm = norm;
  }
  
}

double getSSM(OrthNorm*Z){

  return Z->projP/Z->norm * Z->projP;
}

//function for SNP chisq tests
double runTest(double *geno, double *pheno, int nsample,  double tss_per_n, double* c0, double* c1, double* rss){
  
  static double cov[2][2];
  int df=nsample;

  if(INTERCEPT){
    gsl_fit_linear(geno, 1, pheno, 1, nsample, c0, c1, &cov[0][0], &cov[0][1], &cov[1][1], rss);
    df-=2;
  }else{
    gsl_fit_mul(geno, 1, pheno, 1, nsample, c1,  &cov[1][1], rss);    
    cov[0][0]=-1;
    cov[0][1]=-1;
    *c0=-1;
    df--;
  }
	
  if(gsl_isnan(*rss)){
    *c0=-1;
    *c1=-1;
    *rss = -1;
    return -1;
  }else{
    return (tss_per_n*nsample-(*rss))/(*rss) * df;
  }
}

//function for reading phenotype file
int readPhenotype(char (*indiv_id)[INDIV_ID_LEN], double* indiv_val, int* nas, char* filename){
	
  FILE *fp;
  char s_phenotype[PHENOTYPE_VALUE_LEN];
  char sline[MAX_LINE_WIDTH];
  int nsample;

  fp = fopen(filename, "r");
  nsample=0;
  (*nas) = 0;
	
  while(!feof(fp)){
    strcpy(sline, "");
    fgets(sline, MAX_LINE_WIDTH, fp);
		
    //skip header line
    if(sline[0]=='#') continue;
    //skip blank line
    if(strlen(sline) == 0 ) continue;
		
    int nstatus = sscanf(sline,"%*s %s %*s %*s %*s %s\n", *indiv_id, s_phenotype);
		
    if(nstatus == 2){

      if(s_phenotype[0] == 'N'){
	(*nas)++;
	continue;
      }
      else{
	if(sscanf(s_phenotype, "%lf", indiv_val) != 1){
	  printf("-Wrong format in phenotype %d: %s\n", nsample, sline);
	  abort();
	}
      }
      indiv_id++;
      indiv_val++;
      nsample++;
    }else{
      printf("-Wrong format in line %d: %s\n", nsample, sline);
      abort();
    }
		
  }
  fclose(fp);
  return(nsample);
}

//search through an array for an item
double* array_search(char id[INDIV_ID_LEN], int nsample, char (*indiv_id)[INDIV_ID_LEN],  double* indiv_val){

  int i;

  for(i=0; i<nsample; i++)
    if(strcmp(id, *indiv_id)==0)
      return indiv_val;
    else{
      indiv_val++;
      indiv_id++;
    }

  return NULL;

}

//matching the individuals in phenotype and genotype files
int sortPhenotype(char (*indiv_id)[INDIV_ID_LEN], double* indiv_val, int nsample, double * sorted_phenotype, bool *NA, int * nindiv, char* indiv_id_file){
	
  FILE* fp = fopen(indiv_id_file, "r");
  double* found;
  int i=0;
  char id[INDIV_ID_LEN];
  *nindiv = 0;
  while(!feof(fp)){
    if(fscanf(fp, "%s", id) != 1) break;
    if(strlen(id) == 0)  break;
    found = array_search(id, nsample, indiv_id, indiv_val);
    if(found != NULL){
      sorted_phenotype[i] = *found;
      i++;
      *NA = false;
    }else{
      *NA = true;
    }
    (*nindiv)++;
    NA++;
  }
  fclose(fp);
  printf("-%d out of %d individuals have matching genotype. There are %d total individuals having avaliable genotype.\n", i, nsample, *nindiv);
  return i;
}

//get the phenotype data structure from the file
PHENOTYPE * getPhenotype(char* tfam_file, char* indiv_id_file){

  PHENOTYPE * phenotype = (PHENOTYPE*) malloc(sizeof(PHENOTYPE));
 
  //read phenotype from tfam
  printf("-start to load phenotypes from %s\n", tfam_file);
  
  char indiv_id[MAX_N_INDIV][INDIV_ID_LEN];
  double indiv_val[MAX_N_INDIV];
  int nsample, nindiv, nna;
  
  nsample= readPhenotype(indiv_id, indiv_val, &nna, tfam_file);

  printf("-done loading phenotypes\n");

  nindiv= nsample + nna;

  printf("-%d total individuals, %d have avaliable phenotype\n", nindiv, nsample);

  double * pheno_dat = (double*) malloc (sizeof(double) * nsample);

  printf("-start to load individual IDs from %s\n", indiv_id_file);

  phenotype->N_sample = sortPhenotype(indiv_id, indiv_val, nsample, pheno_dat, phenotype->NA, &phenotype->N_indiv, indiv_id_file);

  printf("-done sorting individuals\n");

  phenotype->pheno_array = gsl_vector_alloc(phenotype->N_sample);

  int i;

  for(i=0; i<phenotype->N_sample; i++){
    gsl_vector_set(phenotype->pheno_array, i, pheno_dat[i]);
  }
  free(pheno_dat);

  phenotype->mean = mean(phenotype->pheno_array->data, phenotype->N_sample);
  phenotype->tss_per_n = tss(phenotype->pheno_array->data, phenotype->N_sample)/phenotype->N_sample;

  printf("-phenotype mean: %f, TSS per indiv: %f\n", phenotype->mean, phenotype->tss_per_n);

  return phenotype;
}

//read a line from the genotype file, format the data and save it in the SNP data structure
int readGeno( char* name, int * chr, double * pos, int * bp, double * MAF, double *R2, double * geno, int nsample, char * sline_geno, char* sline_snp, char * sline_mlinfo,  bool * NA){
  
  int status = sscanf(sline_snp, "%s %d %lg %d", name, chr, pos, bp);
  int i;
  char * nextGeno;
  
  if(status != 4){
    printf("-skipping snp: %s", sline_snp);
    return 0;
  }
  
  if(strlen(sline_mlinfo) == 0) {
    *MAF = 0.5;
    *R2 = 1;
  }else
    sscanf(sline_mlinfo, "%*s %*s %*s %*g %lg %*g %lg", MAF, R2);

    
  for (i=0; i<nsample; i++){
    
    nextGeno = strchr(sline_geno, '\t');
    
    //truncate the string so that sscanf runs much faster
    if(nextGeno!=NULL) *nextGeno='\0';

    if(sscanf(sline_geno, "%lg", geno) != 1){
      printf("-Error in tped file: genotype\n");
      abort();
    }
    
    if(nextGeno!=NULL)
      sline_geno = nextGeno+1;
	
    if(NA[i]) continue;
    geno++;
  }
  return 1;
}

// assign SNPs to gene
int assignSNP2Gene(GENE** startGene, SNP* snp, int curSNP_id, GENE** readyGene, FILE * fp){

  int nReadyGene = 0;
  bool firstRun = true;
  GENE* gene=*startGene;
  for(; gene != NULL; gene = gene->next){
    
    if(gene->skip) continue;
    
    int downstream = gene->bp_end + FLANK;
    int upstream = gene->bp_start - FLANK;
    
    if(snp == NULL){
      *readyGene= gene;
      readyGene++;
      nReadyGene++;
      gene->skip =true;
      continue;
    }
    
    if( snp->chr > gene->chr){
      printf("-SNP %s is on chr %d!!\n", snp->name, snp->chr);
      abort();
    }
    
    //upstream is not decreasing
    if(snp->bp <upstream){
      break;
    }
    else if(snp->bp > downstream){
      *readyGene= gene;
      readyGene++;
      nReadyGene++;
      gene->skip =true;
    }else{
      
      if(snp->eSampleSize > N_HAT_CUTOFF && snp->R2 > R2_CUTOFF){
	//R2 here is the imputation quality
	if(firstRun){
	  firstRun = false;
	  *startGene = gene;
	}
	if(gene->snp_start == -1){
	  gene->snp_start = curSNP_id;
	  gene->nSNP = 0;
	}
	gene->snp_end = curSNP_id;
	gene->nSNP++;
	snp->nGene++;
      }
      fprintf(fp, "%s\t%d\t%g\t%d\t%s\t%s\t%d\t%d\t%g\t%g\t%g\t%g\n", snp->name, snp->chr, snp->pos, snp->bp, gene->ccds, gene->name, gene->bp_start, gene->bp_end, snp->f_stat, snp->MAF, snp->R2, snp->eSampleSize);
      
    }
  }
  
  return nReadyGene;
}

//remove 1 from the nGene counter (tells how many genes this SNP is assigned to).
void cleanSNPinGene(GENE* gene, C_QUEUE* snp_queue){

  static int i;
  static SNP* snp;
  
  for(i = gene->snp_start; i <= gene->snp_end; i++){

    snp = (SNP*) cq_getItem(i, snp_queue);

    snp->nGene--;

  }

}

// remove SNPS that have nGene = 0
void cleanSNPQ(C_QUEUE* snp_queue){

  static SNP* snp;

  //keep at least the most recent SNP
  while(snp_queue->end-snp_queue->start > 1){
    snp = (SNP*)cq_pop(snp_queue,  false);
    
    if(snp->nGene ==0){
      cq_pop(snp_queue,  true);
      if(snp->geno != NULL){
	free(snp->geno);
	snp->geno=NULL;
      }
      
      if(snp->r != NULL){
	free(snp->r);
	snp->r=NULL;
      }
      if(CQ_DEBUG)
	printf("popped, (start, end) = (%lu, %lu)\n", snp_queue->start, snp_queue->end);
      
    }
    else  break;
    
  }
}

//create a SNP data structure
void createSNP(SNP* snp, char* name, double pos, int bp, int chr, double MAF, double R2, double eSampleSize){

  strcpy(snp->name, name);
	
  snp->pos=pos;
  snp->bp=bp;
  snp->chr=chr;
  snp->MAF=MAF;

  snp->R2=R2;
  snp->nGene = 0;
  snp->f_stat=-1;
  snp->nHit=0;
  snp->iPerm=0;
  snp->nHit_bonf=0;
  snp->iPerm_bonf=0;
  snp->eSampleSize = eSampleSize;
  snp->r = NULL;
}

//read SNP from file
SNP* readSNP(C_QUEUE * snp_queue,  FILE * fp_tped, FILE * fp_snp_info, FILE* fp_mlinfo,  PHENOTYPE* phenotype){
	
  static char sline_geno[MAX_LINE_WIDTH];
  static char sline_snp[MAX_LINE_WIDTH];
  static char sline_mlinfo[MAX_LINE_WIDTH];
  static char snp_id[SNP_ID_LEN];
  static int chr;
  static int bp;
  static double pos;
  static double MAF;
  static double R2;
  static SNP* snp = NULL;
  snp = (SNP*) cq_push(snp_queue);
  if(CQ_DEBUG)
    printf("pushed, (start, end) = (%lu, %lu)\n", snp_queue->start, snp_queue->end);

  // to skip blank lines
  while(!feof(fp_tped)){
    strcpy(sline_geno, "");
    fgets(sline_geno, MAX_LINE_WIDTH, fp_tped);
	  
    strcpy(sline_snp, "");
    fgets(sline_snp, MAX_LINE_WIDTH, fp_snp_info);
	  
    strcpy(sline_mlinfo, "");
    if(fp_mlinfo != NULL)
      fgets(sline_mlinfo, MAX_LINE_WIDTH, fp_mlinfo);
	  
    if(strlen(sline_geno) > 0){
      snp->geno=malloc(sizeof(double)*MAX_N_INDIV);
      int status =  readGeno(snp_id, &chr, &pos, &bp, &MAF, &R2, snp->geno, phenotype->N_indiv, sline_geno,sline_snp, sline_mlinfo, phenotype->NA);
	    
      if(status >0 ) {
	createSNP(snp, snp_id, pos, bp, chr, MAF, R2, phenotype->N_sample*R2*2*MAF*(1-MAF));
	return snp;
      }else
	free(snp->geno);

    }
  }
 
  cq_shift(snp_queue);
  if(CQ_DEBUG)
    printf("shifted, (start, end) = (%lu, %lu)\n", snp_queue->start, snp_queue->end);
  return NULL;
}
//create data structure for a gene
GENE* createGene(char* name, char* ccds, int chr, int bp_start, int bp_end, int iPerm){

  GENE* gene = (GENE*) malloc(sizeof(GENE));
  		
  strcpy(gene->name, name);
  strcpy(gene->ccds, ccds);
  gene->bp_start = bp_start;
  gene->bp_end = bp_end;
  gene->chr = chr;
  gene->iPerm = iPerm;
  gene->nSNP = 0;

  gene->next=NULL;
  gene->LD = NULL;
  gene->snp_start = -1;
  gene->snp_end = -1;

  gene->eSNP = -1;
  gene->skip = false;

  gene->maxSSM_SNP = NULL;
  gene->maxBonf_SNP = NULL;
  gene->BF_sum = -1;
  gene->vegas = -1;
  
  return gene;
}
//read genes from file
int readGene(char* refseq_file, GENE** gene, int SNPchr){

  FILE *fp;
  fp = fopen(refseq_file, "r");
  int chr;
  int bp_start;
  int bp_end;
  int iPerm;
  int nGene = 0;
  int prevchr = -1;
  int prevbp = -1;

  char sline[MAX_LINE_WIDTH];
  char ccds[GENE_ID_LEN];
  char name[GENE_ID_LEN];

  GENE* curGene=NULL;
  GENE* prevGene = NULL;

  *gene = createGene("HEAD", "HEAD", -1, -1, -1, -1);
  prevGene = *gene;

  printf("-Time: %s", getTime());
  printf("-start to load genes from %s\n", refseq_file);

  while(!feof(fp)){

    strcpy(sline, "");
    fgets(sline, MAX_LINE_WIDTH, fp); 
    if(strlen(sline)==0)
      continue;

    if(sline[0] == '#'){
      printf("-skipping header: %s", sline);
      continue;
    }
    int nstatus;
    nstatus= sscanf(sline, "%s %s %d %d %d",ccds, name, &chr, &bp_start, &bp_end);
    iPerm =0;
    if(nstatus == 5 ||nstatus == 6 ){

      if(prevchr < chr)
	prevchr = chr;
      else if(prevchr > chr){
	printf("-%s not sorted in chr\n", refseq_file);
	abort();
      }

      if(SNPchr == chr){

	if(prevbp < bp_start)
	  prevbp = bp_start;
	else if(prevbp > bp_start){
	  printf("-%s not sorted in bp!\n", refseq_file);
	  abort();
	}
		
	curGene= createGene(name, ccds, chr, bp_start, bp_end, iPerm);
	prevGene->next = curGene;
	prevGene = curGene;

	nGene++;
      }
    }else{
      printf("-skipping %s", sline);
      continue;
    }
  }
  printf("-done loading genes\n");

  fclose(fp);
  return nGene;
}

//calculate correlation and covariance matrix for SNPs in a gene
void getGeneLD(GENE* gene, gsl_matrix* geneLD, gsl_matrix* geneCov, C_QUEUE * snp_queue, int nsample){
  int i=0, j;
  double ld, cov;
  SNP* snp1, * snp2;
  int snp1_id, snp2_id;
  gene->LD =  geneLD; 
  gene->Cov =  geneCov; 

  snp1 = (SNP*)cq_getItem(gene->snp_start, snp_queue);
  for(snp1_id = gene->snp_start; snp1_id <= gene->snp_end; snp1_id++){
    snp1->gene_id = i;
    cov = variance(snp1->geno, nsample)*nsample;
    gsl_matrix_set(gene->LD, i, i, 1);
    gsl_matrix_set(gene->Cov, i, i, cov);

    j=i+1;
    snp2 = (SNP*)cq_getNext(snp1, snp_queue);
    for(snp2_id = snp1_id+1; snp2_id <= gene->snp_end; snp2_id++){
      ld = correlation(snp1->geno, snp2->geno, nsample);
      cov = covariance(snp1->geno, snp2->geno, nsample)*nsample;
      gsl_matrix_set(gene->LD, i, j, ld);
      gsl_matrix_set(gene->LD, j, i, ld);
      gsl_matrix_set(gene->Cov, i, j, cov);
      gsl_matrix_set(gene->Cov, j, i, cov);

      j++;
      snp2 = cq_getNext(snp2, snp_queue);
    }
    i++;
    snp1 = cq_getNext(snp1, snp_queue);
  }
}
//"partial" forward regression, to be used in Dan's method to calculate the effective number of tests
double forwardRegression(gsl_matrix * LD, gsl_vector * Y, int nSNP, gsl_vector* workspace ){

  double sum=0, weight;
  int i, j, bestSNP;
  
  for(i=0; i<nSNP; i++){
    bestSNP = gsl_vector_max_index(Y);
    weight= gsl_vector_get(Y, bestSNP);
    if(weight <=0) break;
    sum += weight;
    for(j=0; j<nSNP; j++)
      gsl_vector_set(workspace, j, pow(gsl_matrix_get(LD, bestSNP, j), 2));
    gsl_vector_scale(workspace, weight);
    gsl_vector_sub(Y, workspace);
  }
  return sum;
}
//calculate the effective numeber of SNPs using Dan's way
double getGeneESNP_dan(gsl_matrix * LD, int nSNP){

  static double* Y_data = NULL;
  static double* U_data = NULL;
  static gsl_vector_view Y;
  static gsl_vector_view U;
  static int max_dim=MAX_SNP_PER_GENE;
  
  if(U_data == NULL){
    U_data=(double*) malloc(sizeof(double)*MAX_SNP_PER_GENE);
    Y_data=(double*) malloc(sizeof(double)*MAX_SNP_PER_GENE);
  }

  if(nSNP > max_dim){
    max_dim = nSNP;
    free(U_data);
    free(Y_data);
    U_data=(double*) malloc(sizeof(double)*nSNP);
    Y_data=(double*) malloc(sizeof(double)*nSNP);
  }

  Y = gsl_vector_view_array(Y_data, nSNP);
  U = gsl_vector_view_array(U_data, nSNP);

  gsl_vector_set_all(&Y.vector, 1);

  return forwardRegression(LD, &Y.vector, nSNP, &U.vector);
}

//functions for BIC tests
//chech whether the stop condition for permutations is satisfied
bool ifStop(int nHit, int iPerm){
  return ((nHit >= N_CUTOFF) && (iPerm>=N_PERM_MIN)) || (iPerm >=  N_PERM);
}
//print GWiS output
void printBICPermResult(FILE * fp, int i, int nsample, GENE * gene, OrthNorm *Z,  int k_pick, int k_hits, double pval){

  double SSM;
  double f_stat;
  bool summary;
  BIC_STATE* ptr = &gene->bic_state;

  summary = pval >= -EPS;

  SSM =  ptr->RSS[0] - ptr->RSS[i];
  
  if(i > 0 && i <= ptr->iSNP){
    if(summary)
      f_stat = SSM/ptr->RSS[i] * (nsample-i-1)/i;
    else
      f_stat = ptr->bestSNP[i]->f_stat;
  }else
    f_stat = 0;

  //print basic info about the gene
  fprintf(fp, "%d\t%s\t%s\t%d\t%d\t%d\t%d\t%g\t", gene->chr, gene->ccds, gene->name, gene->bp_start, gene->bp_end, gene->bp_end - gene->bp_start+1, gene->nSNP, gene->eSNP);
    
  //print the info about the last snp
  if(summary) 
    fprintf(fp, "SUMMARY\t-\t-\t-\t");
  else
    if(i == 0) 
      fprintf(fp, "NONE\t-\t-\t-\t");
    else if(i <= ptr->iSNP) 
      fprintf(fp, "%s\t%d\t%g\t%g\t", ptr->bestSNP[i]->name, ptr->bestSNP[i]->bp, ptr->bestSNP[i]->MAF,  ptr->bestSNP[i]->R2);
    else fprintf(fp, "-\t-\t-\t-\t");
  
  //print the model info 
  if(i>ptr->iSNP) fprintf(fp, "%d\t-\t-\t-\t-\t", i);
  else{
    if(!summary || i >0)
      fprintf(fp, "%d\t%g\t%g\t%g\t", i, SSM,  ptr->BIC[i] - ptr->BIC[0], f_stat);
    else
      fprintf(fp, "%d\t%g\t%g\t%g\t", i, ptr->RSS[0] - ptr->RSS[1],  ptr->BIC[1] - ptr->BIC[0], ptr->bestSNP[1]->f_stat);

    if(i> 0 && !summary){
      fprintf(fp, "%g\t", 1-Z[ptr->bestSNP[i]->gene_id].norm/Z[ptr->bestSNP[i]->gene_id].norm_original);
    }
    else
      fprintf(fp, "-\t");
  }
  
  //print the model selection info
  fprintf(fp, "%d\t%d\t", k_pick, k_hits);
  if(summary)
    fprintf(fp, "%g\n", pval);
  else
    fprintf(fp, "-\n");
}
//set the counter after each permutation
//1) increase iPerm by 1
//2) incrase Hit by 1 if necessary
//3) enough permutation
bool countBIChit(GENE* gene, int nsample, BIC_STATE* bic_state_new,  FILE* fp_diag){
  
  SNP *curSNP;
  HIT_COUNTER *hits = &gene->hits;
  BIC_STATE * ptr = &gene->bic_state;
  int i, nHit, iPerm;
  bool stop;

  nHit = 0;
  iPerm = 0;
  hits->k_pick[bic_state_new->iSNP]++;
  if(VERBOSE) printf("start to count\n");

  if(bic_state_new->BIC[bic_state_new->iSNP] >= ptr->BIC[ptr->iSNP]-EPS)
    hits->k_hits[bic_state_new->iSNP]++;
  if(bic_state_new->iSNP > hits->maxK)
    hits->maxK = bic_state_new->iSNP;

  for(i=0; i<=hits->maxK; i++){
    nHit += hits->k_hits[i];
    iPerm += hits->k_pick[i];
  }

  stop = ifStop(nHit, iPerm);
  if(VERBOSE)
    if(!stop)
      printf("Bic hits = %d\n", nHit);


  for(i=0; i<=min(bic_state_new->iSNP+1, min(gene->eSNP, MAX_INCLUDED_SNP)) ; i++){
    curSNP = bic_state_new->bestSNP[i];
    if(fp_diag != NULL)
      fprintf(fp_diag, "%d\t%s\t%s\tperm%d\t%d\t%g\t%d\t%d\t%s\t%g\t%g\n", gene->chr, gene->ccds, gene->name, iPerm, gene->nSNP, gene->eSNP,bic_state_new->iSNP, i, (curSNP==NULL)? "NONE": curSNP->name, bic_state_new->BIC[i], bic_state_new->RSS[i]); 
  }

  return stop;
}

// check if a SNP has been selected into the model
bool isSNPUsed(OrthNorm* Z, OrthNorm** bestSNP, int k){
  int i;
  for(i=0; i<k-1; i++)
    if(Z == bestSNP[i])
      return true;
  return false;
}
//check of a SNP has high correlation w/ SNP in the model
bool isSNPCorrelated(OrthNorm* Z, OrthNorm** bestSNP, int k,  gsl_matrix *LD){
  int i;
  for(i=0; i<k-1; i++)
    if(fabs(gsl_matrix_get(LD, Z->snp->gene_id, bestSNP[i]->snp->gene_id)) >VIF_R)
      return true;
  return false;
}
//calculate SSM from ortho-norm 
double calculateMfromZ(OrthNorm*Z, OrthNorm**bestZ, int k, gsl_matrix* LD, gsl_matrix* Cov){

  double SSM;

  if(isSNPUsed(Z, bestZ, k))
    return -1;
  if(isSNPCorrelated(Z, bestZ, k, LD))
    return -1;

  if(VERBOSE)
    printf("Stop1\n");
  
  calculateZ(Z, bestZ, k, Cov);
  if(VERBOSE) 
    printf("Stop2\n");
  if(1-Z->norm/Z->norm_original>VIF_R2)
    return -1;
  if(fabs(Z->norm)<EPS)
    SSM = -1;
  else
    SSM = getSSM(Z);

  return SSM;
}

//get the change of GWiS statistics when adding a SNP into the model
//positive: add
//negative: don't add
double getIncrement(double SSM, double eSNP, int k, int nsample, double RSS){

  //the change in logProb from k-1 to k
  double increment;
  
  increment = log(eSNP-k+1) - log(k);
  increment += log(nsample)/2;

  //increment += log(nsample)/2;
  
  increment += log(RSS - SSM)/2 * nsample;
  increment -= log(RSS)/2 *nsample;
  
  if(k==1)
    increment +=log(eSNP+1) -log((1-P0)/(P0+ (1-P0)/(1+eSNP)));

  return -increment;
}

double getlogBF(double pheno_var, double geno_var, int N, double cov_pheno_geno ){
  double bf=0;
  //  printf("%g, %g, %d, %g\n", pheno_var, geno_var, N, cov_pheno_geno );
  bf=-log10(N)/2-log10(geno_var+1/(N*SIGMA_A*SIGMA_A))/2-log10(SIGMA_A);
  //  printf("%g\n", bf);
  bf+= -((double)N)/2 * log10(1-cov_pheno_geno*cov_pheno_geno/(geno_var*pheno_var+pheno_var/N/SIGMA_A/SIGMA_A));
  return bf;
}

//find the best fit snp to add to the GWiS model for the gene
SNP* bestFitSNP(OrthNorm **bestZ, OrthNorm *Z, int k, GENE* gene, double *SSM, int nsample, double RSS, double BIC_prev, double SSM_prev, double*increment, FILE* fp_diag){
 
  SNP* ret=NULL;
  int i, j;
  ret = NULL;
  double maxSSM=0;
  double maxIncrease = -1;

  for(i=0; i<gene->nSNP; i++, Z++){

    if(VERBOSE) printf("working on %s\n", Z->snp->name);
	
    *SSM= calculateMfromZ(Z, bestZ, k, gene->LD, gene->Cov)/nsample;
    //	double beta =  Z->projP/Z->norm;
    //	double se2 = (225.265-*SSM-SSM_prev)/Z->norm;
    //	printf("SE2 = %g, z=%g, SSM=%g\n", se2, beta/sqrt(se2),*SSM+SSM_prev);
    //	printf("%s\t%g\n", Z->snp->name, *SSM);
    if(*SSM<0) continue;
    *increment = getIncrement(*SSM, gene->eSNP, k, nsample, RSS);

    if(VERBOSE) printf("Stop3\n");
    if(fp_diag != NULL){
      fprintf(fp_diag, "%d\t%s\t%s\t%d\t%g\t%d\t", gene->chr, gene->ccds,gene->name, gene->nSNP, gene->eSNP, k); 
      for(j=1; j<k; j++)
	fprintf(fp_diag, "%s_", bestZ[j-1]->snp->name);
      fprintf(fp_diag, "%s\t", Z->snp->name);
      fprintf(fp_diag, "%g\t%g\t%g\n", *increment+BIC_prev, *SSM+SSM_prev, 1-Z->norm/Z->norm_original);
    }
    if(VERBOSE) printf("Stop4\n");
    if(*SSM>maxSSM){
      ret=Z->snp;
      //	  printf("changed to %s at %g and %g \n", ret->name, *SSM, maxSSM);
      maxSSM = *SSM;
      maxIncrease = *increment;
      bestZ[k-1] = Z;
    }
  }
  if(VERBOSE) printf("Stop5\n");
  (*SSM) = maxSSM;
  (*increment) = maxIncrease;

  return ret;
}

//get the constant term in GWiS test statistics
double getK0(int nsample, double pheno_mean, double tss_per_n, double v){

  double ret =0;
  ret +=  log(tss_per_n) / 2;
  ret += 0.5;
  ret *= nsample;
  ret -= log(P0+ (1-P0)/(1+v));
  return -ret;
}
//to build the GWiS model by finding the best SNP and decide whether to add it or stop adding SNPs
bool calBestFitSNP( BIC_STATE * bic_state, int k, OrthNorm** bestZ, GENE* gene,  OrthNorm *Z, int nsample, FILE * fp_result, FILE * fp_diag){

  double increment, SSM, BIC;
  SNP *bestSNP;

  bestSNP=bestFitSNP(bestZ, Z, k, gene, &SSM, nsample,(bic_state->RSS)[k-1],(bic_state->BIC)[k-1],(bic_state->RSS)[0]-(bic_state->RSS)[k-1], &increment, fp_diag);

  BIC = increment + (bic_state->BIC)[k-1];
  
  (bic_state->bestSNP)[k] =bestSNP;
  (bic_state->RSS)[k] = (bic_state->RSS)[k-1]-SSM;
  (bic_state->BIC)[k] = BIC;

  if(increment < 0 && k>K_MIN){
    bic_state->iSNP = k-1;
    return false; //stop adding SNPs
  }else
    bic_state->iSNP = k;

  return true;
}

//initialize the GWiS result data structure
void initBIC_State(BIC_STATE*  bic_state, double pheno_tss_per_n){

  int k;
  for (k=0; k<=MAX_INCLUDED_SNP; k++){
    bic_state->bestSNP[k]=NULL;
    bic_state->BIC[k]=-1;
    bic_state->RSS[k]=-1;
  }
  bic_state->iSNP = 0;
  (bic_state->RSS)[0] =  pheno_tss_per_n;
  (bic_state->BIC)[0] = 0;
}


// make a copy of the ortho-norm array
void copyZ(OrthNorm *Z_src, OrthNorm *Z_dest, int n){

  int i, k;
  for (i = 0; i < n; i++, Z_src++, Z_dest++){ 
    Z_dest->k = Z_src->k;
    Z_dest->snp = Z_src->snp;
    Z_dest->projP = Z_src->projP;
    Z_dest->norm  = Z_src->norm;
    Z_dest->norm_original  = Z_src->norm_original;
    for (k=0; k<MAX_INCLUDED_SNP; k++)
      Z_dest->sum_X_bestZ[k]  = Z_src->sum_X_bestZ[k];
  }
}

// used for full model search
void compareModels(double* SSM, double *BIC, double *bestBIC, OrthNorm **bestZ, int k, BIC_STATE * bic_state, GENE * gene, OrthNorm *Z, FILE* fp_diag){
  
  int i;
  double totalSSM=0;

  if(BIC[k-1] > *bestBIC){
    bic_state->iSNP = k;
    *bestBIC=BIC[k-1];

    for(i=1; i<=k; i++){
      totalSSM += SSM[i-1];
      if(i==k){
	(bic_state->bestSNP)[i] = Z->snp;
      }else{
	(bic_state->bestSNP)[i] =bestZ[i-1]->snp;
      }
      (bic_state->RSS)[i] = (bic_state->RSS)[i-1]-SSM[i-1];
      (bic_state->BIC)[i] = BIC[i-1];
    }
    if(fp_diag!=NULL){
      fprintf(fp_diag, "%d\t%s\t%s\t%d\t%g\t%d\t", gene->chr, gene->ccds,gene->name, gene->nSNP, gene->eSNP, k); 
      for(i=1; i<k; i++)
	fprintf(fp_diag, "%s_", bestZ[i-1]->snp->name);
      fprintf(fp_diag, "%s\t", Z->snp->name);
      fprintf(fp_diag, "%g\t%g\t%g\n", BIC[k-1], totalSSM, 1-Z->norm/Z->norm_original);
    }
  }
}

//full model search up to 3 SNPs
BIC_STATE* runBIC_fullSearch3(GENE* gene, OrthNorm *Z, BIC_STATE * bic_state, int nsample, double pheno_mean, double pheno_tss_per_n, FILE* fp_result, FILE * fp_diag, bool mute){

  int i, j, k;
  double SSM[3];
  double increment[3];
  double BIC[3];
  double bestBIC;
  OrthNorm *bestZ[MAX_INCLUDED_SNP];

  OrthNorm *Z_backup2 = (OrthNorm*) malloc(sizeof(OrthNorm)* gene->nSNP);
  OrthNorm *Z_backup3 = (OrthNorm*) malloc(sizeof(OrthNorm)* gene->nSNP);
  
  initBIC_State(bic_state, pheno_tss_per_n);
  //getK0(nsample, pheno_mean, pheno_tss_per_n, gene->eSNP);

  if(fp_result != NULL)
    fprintf(fp_result, "%d\t%s\t%s\t%d\t%d\t%d\t%d\t%g\t%s\t%s\t%s\t%s\t%d\t%g\t%g\t%g\t-\n", gene->chr, gene->ccds, gene->name, gene->bp_start, gene->bp_end, gene->bp_end-gene->bp_start+1, gene->nSNP, gene->eSNP,"NONE", "-", "-", "-", 0, 0.0, 0.0,0.0); 

  bestBIC = 0;

  for(i=0; i<gene->nSNP; i++){

    if(VERBOSE)
      printf("working on %s as snp1\n", Z[i].snp->name);
    SSM[0] = calculateMfromZ(&Z[i], bestZ, 1, gene->LD, gene->Cov)/nsample;
	
    if(SSM[0] < 0)
      continue;

    increment[0] = getIncrement(SSM[0], gene->eSNP, 1, nsample, (bic_state->RSS)[0]);
    BIC[0] = increment[0] + (bic_state->BIC)[0];
    compareModels(SSM, BIC, &bestBIC, bestZ, 1, bic_state, gene, &Z[i], fp_diag);

    if(gene->eSNP >=2){

      bestZ[0]=&Z[i];
      copyZ(Z, Z_backup2, i);
      for(j=0; j<i; j++){	

	if(VERBOSE)
	  printf("working on %s as snp2\n", Z[j].snp->name);

	SSM[1] = calculateMfromZ(&Z[j], bestZ, 2, gene->LD, gene->Cov)/nsample;

	if(SSM[1] < 0)
	  continue;

	increment[1] = getIncrement(SSM[1], gene->eSNP, 2, nsample, (bic_state->RSS)[0]-SSM[0]);
	BIC[1] = increment[0] +increment[1] + (bic_state->BIC)[0];	
	compareModels(SSM, BIC, &bestBIC, bestZ, 2, bic_state, gene, &Z[j], fp_diag);
		
	if(gene->eSNP >=3){
	  bestZ[1]=&Z[j];
	  copyZ(Z, Z_backup3, j);
	  for(k=0; k<j; k++){

	    if(VERBOSE)
	      printf("working on %s as snp3\n", Z[k].snp->name);

	    SSM[2] = calculateMfromZ(&Z[k], bestZ, 3, gene->LD, gene->Cov)/nsample;
			  
	    if(SSM[2] < 0)
	      continue;

	    increment[2] = getIncrement(SSM[2], gene->eSNP, 3, nsample, (bic_state->RSS)[0]-SSM[0]-SSM[1]);
	    BIC[2] = increment[0] + increment[1] + increment[2] + (bic_state->BIC)[0];	
	    compareModels(SSM, BIC, &bestBIC, bestZ, 3, bic_state, gene, &Z[k], fp_diag);
	  }
	  copyZ(Z_backup3, Z, j);
	}
      }
      copyZ(Z_backup2, Z, i);
    }
  }
  
  for(k=1; k<=bic_state->iSNP; k++){

    for(i=0; i< gene->nSNP; i++)
      calculateMfromZ(&Z[i], bestZ, k, gene->LD, gene->Cov);
    bestZ[k-1] = &Z[bic_state->bestSNP[k]->gene_id];

    if(fp_result != NULL)
      fprintf(fp_result, "%d\t%s\t%s\t%d\t%d\t%d\t%d\t%g\t%s\t%d\t%g\t%g\t%d\t%g\t%g\t%g\t%g\n", gene->chr, gene->ccds, gene->name, gene->bp_start, gene->bp_end, gene->bp_end-gene->bp_start+1, gene->nSNP, gene->eSNP, bic_state->bestSNP[k]->name,bic_state->bestSNP[k]->bp, bic_state->bestSNP[k]->MAF,bic_state->bestSNP[k]->R2, k ,bic_state->RSS[0] - bic_state->RSS[k], bic_state->BIC[k], bic_state->bestSNP[k]->f_stat, 1-bestZ[k-1]->norm/bestZ[k-1]->norm_original); 

  }
  
  
  if(fp_result != NULL){
    double f_stat = bic_state->iSNP==0? 0:(bic_state->RSS[0]-bic_state->RSS[bic_state->iSNP])/bic_state->RSS[bic_state->iSNP] * (nsample-bic_state->iSNP-1)/bic_state->iSNP;
  
    fprintf(fp_result, "%d\t%s\t%s\t%d\t%d\t%d\t%d\t%g\t%s\t%s\t%s\t%s\t%d\t%g\t%g\t%g\t-\n", gene->chr, gene->ccds, gene->name, gene->bp_start, gene->bp_end, gene->bp_end-gene->bp_start+1, gene->nSNP, gene->eSNP,"SUMMARY", "-", "-", "-", bic_state->iSNP,bic_state->RSS[0] - bic_state->RSS[bic_state->iSNP], bic_state->BIC[bic_state->iSNP],f_stat); 
  }

  if(bic_state->iSNP == 3){
    int k;
    for(k=4; k<= min(gene->eSNP, MAX_INCLUDED_SNP);k++){
      if(!calBestFitSNP(bic_state, k, bestZ, gene,  Z, nsample, fp_result, fp_diag))
	break;
    }
  }

  free(Z_backup2);
  free(Z_backup3);

  return bic_state;
}

//call GWiS core function 
BIC_STATE* runBIC(GENE* gene, OrthNorm *Z, BIC_STATE * bic_state, int nsample, double pheno_mean, double pheno_tss_per_n, FILE* fp_result, FILE * fp_diag, bool mute){
  //inputs
  //gene, Z, nsample, pheno_mean, pheno_tss_per_n, mute, fp_result, fp_diag
  //output
  //bic_state

  int k;

  OrthNorm *bestZ[MAX_INCLUDED_SNP];

  initBIC_State(bic_state, pheno_tss_per_n);

  //getK0(nsample, pheno_mean, pheno_tss_per_n, gene->eSNP);

  if(fp_result != NULL)
    fprintf(fp_result, "%d\t%s\t%s\t%d\t%d\t%d\t%d\t%g\t%s\t%s\t%s\t%s\t%d\t%g\t%g\t%g\t-\n", gene->chr, gene->ccds, gene->name, gene->bp_start, gene->bp_end, gene->bp_end-gene->bp_start+1, gene->nSNP, gene->eSNP,"NONE", "-", "-", "-", 0, 0.0, 0.0,0.0); 
 
  if(VERBOSE) printf("finished init\n");

  for(k=1; k<= min(gene->eSNP, MAX_INCLUDED_SNP);k++){
    if(!calBestFitSNP(bic_state, k, bestZ, gene,  Z, nsample, fp_result, fp_diag))
      break;
  }
  
  if(fp_result != NULL){
    for(k=1; k<=bic_state->iSNP; k++){
      fprintf(fp_result, "%d\t%s\t%s\t%d\t%d\t%d\t%d\t%g\t%s\t%d\t%g\t%g\t%d\t%g\t%g\t%g\t%g\n", gene->chr, gene->ccds, gene->name, gene->bp_start, gene->bp_end, gene->bp_end-gene->bp_start+1, gene->nSNP, gene->eSNP, bic_state->bestSNP[k]->name,bic_state->bestSNP[k]->bp, bic_state->bestSNP[k]->MAF,bic_state->bestSNP[k]->R2, k ,bic_state->RSS[0] - bic_state->RSS[k], bic_state->BIC[k], bic_state->bestSNP[k]->f_stat, 1-bestZ[k-1]->norm/bestZ[k-1]->norm_original); 
    }
    double f_stat = bic_state->iSNP==0? (bic_state->RSS[0]-bic_state->RSS[1])/bic_state->RSS[1] * (nsample-2):(bic_state->RSS[0]-bic_state->RSS[bic_state->iSNP])/bic_state->RSS[bic_state->iSNP] * (nsample-bic_state->iSNP-1)/bic_state->iSNP;
	
    fprintf(fp_result, "%d\t%s\t%s\t%d\t%d\t%d\t%d\t%g\t%s\t%s\t%s\t%s\t%d\t%g\t%g\t%g\t-\n", gene->chr, gene->ccds, gene->name, gene->bp_start, gene->bp_end, gene->bp_end-gene->bp_start+1, gene->nSNP, gene->eSNP,"SUMMARY", "-", "-", "-", bic_state->iSNP, bic_state->iSNP==0? bic_state->RSS[0] - bic_state->RSS[1]:bic_state->RSS[0] - bic_state->RSS[bic_state->iSNP], bic_state->iSNP==0?bic_state->BIC[1]:bic_state->BIC[bic_state->iSNP],f_stat); 
  }

  return bic_state;
}

//perform permutations for all methods
void runBIC_thread_wrap(void* ptr){
  
  static BIC_THREAD_DATA * thread_param =NULL;
  thread_param = (BIC_THREAD_DATA * ) ptr;

  static BIC_STATE  bic_state;
  static SNP * snp;
  static int i;
  static double bestPval,bestPval_bonf, bestFstat, bestFstat_bonf, bestSSM, currentSSM;
  static bool ifStop_BIC, ifStop_SNP, ifStop_SNP_bonf, ifStop_SNP_local, ifStop_SNP_bonf_local, ifStop_BF, ifStop_VEGAS;

  static OrthNorm *Z = NULL;
  static int max_dim = MAX_SNP_PER_GENE;

  if(Z==NULL)
    Z= (OrthNorm*) malloc(sizeof(OrthNorm)* MAX_SNP_PER_GENE);

  if(thread_param->gene->nSNP > max_dim){
    max_dim = thread_param->gene->nSNP;
    free(Z);
    Z= (OrthNorm*) malloc(sizeof(OrthNorm)*max_dim);
  }

  //don't permute for GWiS if K  (iSNP) =1
  if(GET_GENE_PVAL && thread_param->gene->bic_state.iSNP > 0)
    ifStop_BIC = false;
  else
    ifStop_BIC = true;

  if(GET_SNP_PVAL)
    ifStop_SNP = false;
  else
    ifStop_SNP = true;

  if(GET_SNP_PERM_PVAL)
    ifStop_SNP_bonf = false;
  else
    ifStop_SNP_bonf = true;

  if(GET_VEGAS_PVAL)
    ifStop_VEGAS = false;
  else
    ifStop_VEGAS = true;

  if(GET_BF_PVAL)
    ifStop_BF = false;
  else
    ifStop_BF = true;

  //  printf("-Time: %s", getTime());
  while(!ifStop_BIC||!ifStop_SNP||!ifStop_SNP_bonf||!ifStop_BF||!ifStop_VEGAS){
    shuffle(thread_param->pheno, thread_param->nsample, thread_param->r, NULL, "\t");
    snp = (SNP*) cq_getItem(thread_param->gene->snp_start, thread_param ->snp_queue);
    for(i= thread_param->gene->snp_start; i<= thread_param->gene->snp_end; i++){
      if(snp->iPerm == 0 ||!ifStop_SNP_bonf|| !ifStop_BIC||!ifStop_VEGAS||!ifStop_BF){
	Z[snp->gene_id].projP= covariance(snp->geno, thread_param->pheno->data, thread_param->nsample) *thread_param->nsample;
	Z[snp->gene_id].norm = snp->geno_tss;
	Z[snp->gene_id].norm_original = snp->geno_tss;
	Z[snp->gene_id].snp = snp;
      }
      snp = cq_getNext(snp,thread_param ->snp_queue);
    }

    if(!ifStop_SNP_bonf){
      if(VERBOSE)
	printf("running SNP Bonf\n");
      ifStop_SNP_bonf = true;
      snp = (SNP*) cq_getItem(thread_param->gene->snp_start, thread_param ->snp_queue);
      bestPval_bonf =2;
      bestFstat_bonf =-1;
      bestSSM = 0;
      //get the best SSM for bonf correction
      for(i= thread_param->gene->snp_start; i<= thread_param->gene->snp_end; i++){
	// has to run before GWiS, otherwise projP and norm will be changed
	currentSSM = pow(Z[snp->gene_id].projP, 2)/Z[snp->gene_id].norm; 
	if(currentSSM > bestSSM){
	  bestSSM = currentSSM;
	}
	snp = cq_getNext(snp,thread_param ->snp_queue);
      }

      snp = (SNP*) cq_getItem(thread_param->gene->snp_start, thread_param ->snp_queue);
      for(i= thread_param->gene->snp_start; i<= thread_param->gene->snp_end; i++){

	if(snp->iPerm_bonf == 0){

	  if(pow(snp->sum_pheno_geno,2)/snp->geno_tss <= bestSSM )
	    thread_param->gene->hits.hit_snp_bonf[snp->gene_id]++;

	  ifStop_SNP_bonf_local = ifStop(thread_param->gene->hits.hit_snp_bonf[snp->gene_id], thread_param->gene->hits.perm_snp_bonf[snp->gene_id]+1);
		  
	  if(ifStop_SNP_bonf_local){
	    snp->iPerm_bonf =  thread_param->gene->hits.perm_snp_bonf[snp->gene_id]+1;
	    snp->nHit_bonf =  thread_param->gene->hits.hit_snp_bonf[snp->gene_id];
	  }
	  ifStop_SNP_bonf=ifStop_SNP_bonf&&ifStop_SNP_bonf_local;
	  if(VERBOSE)
	    if(!ifStop_SNP_bonf_local)
	      printf("%s, %g, %d\n", snp->name, snp->f_stat, thread_param->gene->hits.hit_snp_bonf[snp->gene_id]);
	}

	thread_param->gene->hits.perm_snp_bonf[snp->gene_id]++;

	if(snp->iPerm_bonf> 0 && ((bestPval_bonf > (double)snp->nHit_bonf/snp->iPerm_bonf)|| (fabs(bestPval_bonf - (double)snp->nHit_bonf/snp->iPerm_bonf) < EPS && snp->f_stat > bestFstat_bonf )) ){
	  thread_param->gene->maxBonf_SNP = snp;
	  bestPval_bonf =  (double)snp->nHit_bonf/snp->iPerm_bonf;
	  bestFstat_bonf = snp->f_stat;
	}

	snp = cq_getNext(snp,thread_param ->snp_queue);
      }

      if(VERBOSE)
	printf("end snp bonf\n");

    }


    if(!ifStop_SNP){
      if(VERBOSE)
	printf("running SNP\n");
      ifStop_SNP = true;
      snp = (SNP*) cq_getItem(thread_param->gene->snp_start, thread_param ->snp_queue);
      bestPval =2;
      bestFstat =-1;

      for(i= thread_param->gene->snp_start; i<= thread_param->gene->snp_end; i++){
	if(snp->iPerm == 0){
	  if(fabs(snp->sum_pheno_geno) <= fabs(Z[snp->gene_id].projP) )
	    thread_param->gene->hits.hit_snp[snp->gene_id]++;
	  ifStop_SNP_local = ifStop(thread_param->gene->hits.hit_snp[snp->gene_id], thread_param->gene->hits.perm_snp[snp->gene_id]+1);

	  if(ifStop_SNP_local){
	    snp->iPerm =  thread_param->gene->hits.perm_snp[snp->gene_id]+1;
	    snp->nHit =  thread_param->gene->hits.hit_snp[snp->gene_id];
	  }
	  ifStop_SNP=ifStop_SNP&&ifStop_SNP_local;
	  if(VERBOSE)
	    if(!ifStop_SNP_local)
	      printf("%s, %g, %d\n", snp->name, snp->f_stat, thread_param->gene->hits.hit_snp[snp->gene_id]);
	}
	thread_param->gene->hits.perm_snp[snp->gene_id]++;

	if(snp->iPerm> 0 && ((bestPval > (double)snp->nHit/snp->iPerm)|| (fabs(bestPval - (double)snp->nHit/snp->iPerm) < EPS &&  snp->f_stat > bestFstat )) ){
	  thread_param->gene->maxSSM_SNP = snp;
	  bestPval =  (double)snp->nHit/snp->iPerm;
	  bestFstat = snp->f_stat;
	}

	snp = cq_getNext(snp,thread_param ->snp_queue);
      }
      if(VERBOSE)
	printf("end snp\n");
    }

     if(!ifStop_BF){
      if(VERBOSE)
	printf("running Bayes Factors\n");
      double bf_sum=0;
      snp = (SNP*) cq_getItem(thread_param->gene->snp_start, thread_param ->snp_queue);
      for(i= thread_param->gene->snp_start; i<= thread_param->gene->snp_end; i++){
	// has to run before GWiS, otherwise projP and norm will be changed
	bf_sum += pow(10, getlogBF(thread_param->pheno_tss_per_n, Z[snp->gene_id].norm_original/thread_param->nsample,thread_param->nsample, Z[snp->gene_id].projP/thread_param->nsample));
	snp = cq_getNext(snp,thread_param ->snp_queue);
	//	    printf("DEBUG, %d, %s, %g\n",  thread_param->gene->hits.perm_bf, snp->name,  getlogBF(thread_param->pheno_tss_per_n, Z[snp->gene_id].norm_original/thread_param->nsample,thread_param->nsample, Z[snp->gene_id].projP/thread_param->nsample));
      }
      if(bf_sum >= thread_param->gene->BF_sum){
	thread_param->gene->hits.hit_bf++;
      }
	  
      ifStop_BF=ifStop(thread_param->gene->hits.hit_bf, thread_param->gene->hits.perm_bf+1);
      thread_param->gene->hits.perm_bf++;
    }

    if(!ifStop_VEGAS){
      if(VERBOSE)
	printf("running VEGAS\n");
      double vegas=0;
      snp = (SNP*) cq_getItem(thread_param->gene->snp_start, thread_param ->snp_queue);
      for(i= thread_param->gene->snp_start; i<= thread_param->gene->snp_end; i++){
	// has to run before GWiS, otherwise projP and norm will be changed
	vegas += pow(Z[snp->gene_id].projP, 2)/Z[snp->gene_id].norm/thread_param->pheno_tss_per_n/thread_param->nsample*(thread_param->nsample-2);
	snp = cq_getNext(snp,thread_param ->snp_queue);
      }
      if(vegas >= thread_param->gene->vegas){
	thread_param->gene->hits.hit_vegas++;
      }
      ifStop_BF=ifStop(thread_param->gene->hits.hit_vegas, thread_param->gene->hits.perm_vegas+1);
      thread_param->gene->hits.perm_vegas++;
    }

    if(!ifStop_BIC){
      if(VERBOSE)
	printf("running BIC\n");

      if(FULLSEARCH)
	//FULLSEARCH is for full model search up to 3 SNPs
	runBIC_fullSearch3(thread_param->gene, Z, &bic_state, thread_param->nsample, thread_param->pheno_mean, thread_param->pheno_tss_per_n, NULL, NULL, true);
      else
	runBIC(thread_param->gene, Z, &bic_state, thread_param->nsample, thread_param->pheno_mean, thread_param->pheno_tss_per_n, NULL, NULL, true);
		
      ifStop_BIC = countBIChit(thread_param->gene, thread_param->nsample, &bic_state, NULL);
      if(VERBOSE)
	printf("end BIC\n");
    }

  }

}

//initialize the permutation counters
void initHit(GENE* gene,  C_QUEUE * snp_queue){

  zeroArray_int(gene->hits.k_pick, MAX_INCLUDED_SNP+1);
  zeroArray_int(gene->hits.k_hits, MAX_INCLUDED_SNP+1);
  gene->hits.maxK = gene->bic_state.iSNP;

  gene->hits.hit_bf = 0;
  gene->hits.perm_bf = 0;
  gene->hits.hit_vegas = 0;
  gene->hits.perm_vegas = 0;
  gene->hits.hit_bic = 0;
  gene->hits.perm_bic = 0;

  SNP* snp = cq_getItem( gene->snp_start, snp_queue);
  int i;
  for(i= gene->snp_start; i <= gene->snp_end; i++){
    gene->hits.hit_snp[snp->gene_id] =  0;
    gene->hits.perm_snp[snp->gene_id] =  0;
    gene->hits.hit_snp_bonf[snp->gene_id] =  0;
    gene->hits.perm_snp_bonf[snp->gene_id] =  0;
    snp = cq_getNext(snp, snp_queue);
  }
}

//setup the scratch space for the methods
//get the model for the original trait
//if K>1, run permutations
void getPerm(C_QUEUE * snp_queue, GENE* gene, PHENOTYPE * phenotype, gsl_rng *r, FILE* fp_run_bic_result, FILE* fp_BIC_result, FILE* fp_SNP_result,FILE* fp_SNP_perm_result, FILE* fp_BF_result, FILE* fp_VEGAS_result, FILE * fp_BIC_perm_diag,  FILE * fp_SNP_diag){

  static gsl_vector* pheno_perm= NULL;
  if(pheno_perm == NULL)
    pheno_perm = gsl_vector_alloc(phenotype->N_sample);

  static BIC_THREAD_DATA thread_param;

  static int max_dim=MAX_SNP_PER_GENE;
  static OrthNorm *Z = NULL;
  static int* hit_snp = NULL;
  static int* perm_snp = NULL;
  static int* hit_snp_bonf = NULL;
  static int* perm_snp_bonf = NULL;
  static int nHit, nPerm, i;
  static SNP* snp;

  if(Z == NULL){
    Z = malloc(sizeof(OrthNorm)*MAX_SNP_PER_GENE);
    hit_snp = malloc(sizeof(int)*MAX_SNP_PER_GENE);
    perm_snp = malloc(sizeof(int)*MAX_SNP_PER_GENE);
    hit_snp_bonf = malloc(sizeof(int)*MAX_SNP_PER_GENE);
    perm_snp_bonf = malloc(sizeof(int)*MAX_SNP_PER_GENE);
  }
  if(gene->nSNP > max_dim){
    max_dim = gene->nSNP;
    free(Z);
    free(hit_snp);
    free(perm_snp);
    free(hit_snp_bonf);
    free(perm_snp_bonf);
    Z = (OrthNorm*) malloc(sizeof(OrthNorm)*max_dim);
    hit_snp = malloc(sizeof(int)*max_dim);
    perm_snp = malloc(sizeof(int)*max_dim);
    hit_snp_bonf = malloc(sizeof(int)*max_dim);
    perm_snp_bonf = malloc(sizeof(int)*max_dim);
  }
  
  //initialized the orthnom vectors, equavalent to projecting all the snps to zo (vector of 1's)
 
  if(GET_BF_PVAL){
    gene->BF_sum=0;
  }
  
  if(GET_VEGAS_PVAL){
    gene->vegas=0;
  }
  
  snp = cq_getItem(gene->snp_start, snp_queue);
  
  for(i= gene->snp_start; i <= gene->snp_end; i++){
    Z[snp->gene_id].projP= snp->sum_pheno_geno;
    Z[snp->gene_id].norm = snp->geno_tss;
    Z[snp->gene_id].norm_original = snp->geno_tss;
    Z[snp->gene_id].snp = snp;

    if(GET_BF_PVAL){
      gene->BF_sum+=pow(10, snp->BF);
    }

    if(GET_VEGAS_PVAL){
      gene->vegas+=snp->f_stat;
    }

    snp = cq_getNext(snp, snp_queue);
  }

  //compute the model for the GWiS
  if(FULLSEARCH)
    runBIC_fullSearch3(gene, Z, &gene->bic_state, phenotype->N_sample, phenotype->mean, phenotype->tss_per_n, fp_run_bic_result, NULL , false);
  else
    runBIC(gene, Z, &gene->bic_state, phenotype->N_sample, phenotype->mean, phenotype->tss_per_n, fp_run_bic_result, NULL, false);

  if(VERBOSE)
    printf("%s: start=%d, end=%d, eSNP=%g, iSNP=%d\n", gene->ccds, gene->bp_start, gene->bp_end, gene->eSNP, gene->bic_state.iSNP);

  //Want permutations?
  if(!SKIPPERM){
    gsl_vector_memcpy(pheno_perm, phenotype->pheno_array);
    //initialize each gene
    gene->hits.hit_snp = hit_snp;
    gene->hits.perm_snp = perm_snp;
    gene->hits.hit_snp_bonf = hit_snp_bonf;
    gene->hits.perm_snp_bonf = perm_snp_bonf;
    initHit(gene, snp_queue);

    thread_param.pheno = pheno_perm;
    thread_param.pheno_mean = phenotype->mean;
    thread_param.pheno_tss_per_n = phenotype->tss_per_n;
    thread_param.nsample = phenotype->N_sample;
    thread_param.r = r;
    thread_param.gene = gene;
    thread_param.snp_queue = snp_queue;
    thread_param.fp_SNP_diag = fp_SNP_diag; 
	
    if(VERBOSE)
      printf("start to run perm\n");
	
    //comment out this line if you don't want permutations
    runBIC_thread_wrap(&thread_param);
	
    if(VERBOSE)
      printf("perm finished\n");
	
    if(GET_GENE_PVAL){
      nHit=0;
      nPerm=0;
      for(i=0; i<=gene->hits.maxK; i++){
	printBICPermResult(fp_BIC_result, i, phenotype->N_sample, gene, Z, gene->hits.k_pick[i], gene->hits.k_hits[i], -1);
	nHit += gene->hits.k_hits[i];
	nPerm += gene->hits.k_pick[i];
      }

      printBICPermResult(fp_BIC_result, gene->bic_state.iSNP,  phenotype->N_sample, gene, Z, nPerm, nHit, nPerm==0 ? 1 : (((double)nHit) / nPerm));
	
      if(VERBOSE)
	printf("print bic finished\n");
    }
	
    //for minSNP
    if(GET_SNP_PVAL){
      snp = cq_getItem(gene->snp_start, snp_queue);
      for(i= gene->snp_start; i <= gene->snp_end; i++){
	if(fp_SNP_result != NULL){
	  fprintf(fp_SNP_result, "%d\t%s\t%s\t%d\t%d\t%d\t%d\t%g\t", gene->chr, gene->ccds, gene->name, gene->bp_start, gene->bp_end, gene->bp_end - gene->bp_start+1, gene->nSNP, gene->eSNP);
	  fprintf(fp_SNP_result, "%s\t%d\t%g\t%g\t%g\t%d\t%d\t%g\t%d\n", snp->name,snp->bp, snp->MAF, snp->R2, snp->f_stat, snp->iPerm, snp->nHit, (double)snp->nHit/snp->iPerm, gene->maxSSM_SNP == snp? 1: 0);
	}
	snp = cq_getNext(snp, snp_queue);
      }
      if(VERBOSE)
	printf("print SNP finished\n");
    }

    //for minSNP=P
    if(GET_SNP_PERM_PVAL){
      snp = cq_getItem(gene->snp_start, snp_queue);
      for(i= gene->snp_start; i <= gene->snp_end; i++){
	//make change
	if(fp_SNP_perm_result != NULL){
	  fprintf(fp_SNP_perm_result, "%d\t%s\t%s\t%d\t%d\t%d\t%d\t%g\t", gene->chr, gene->ccds, gene->name, gene->bp_start, gene->bp_end, gene->bp_end - gene->bp_start+1, gene->nSNP, gene->eSNP);
	  fprintf(fp_SNP_perm_result, "%s\t%d\t%g\t%g\t%g\t%d\t%d\t%g\t%d\n", snp->name,snp->bp, snp->MAF, snp->R2, snp->f_stat, snp->iPerm_bonf, snp->nHit_bonf, (double)snp->nHit_bonf/snp->iPerm_bonf, gene->maxBonf_SNP == snp? 1: 0);
	}
	snp->nHit_bonf=0;
	snp->iPerm_bonf=0;
	snp = cq_getNext(snp, snp_queue);
      }
      if(VERBOSE)
	printf("print SNP bonf finished\n");
    }

    if(GET_BF_PVAL){
      if(fp_BF_result != NULL){
	fprintf(fp_BF_result, "%d\t%s\t%s\t%d\t%d\t%d\t%d\t%g\t", gene->chr, gene->ccds, gene->name, gene->bp_start, gene->bp_end, gene->bp_end - gene->bp_start+1, gene->nSNP, gene->eSNP);
	fprintf(fp_BF_result, "%g\t%d\t%d\t%g\n", gene->BF_sum, gene->hits.perm_bf, gene->hits.hit_bf, ((double)gene->hits.hit_bf)/(gene->hits.perm_bf));
      }
      if(VERBOSE)
	printf("print BF finished\n");
    }
    if(GET_VEGAS_PVAL){
      if(fp_VEGAS_result != NULL){
	fprintf(fp_VEGAS_result, "%d\t%s\t%s\t%d\t%d\t%d\t%d\t%g\t", gene->chr, gene->ccds, gene->name, gene->bp_start, gene->bp_end, gene->bp_end - gene->bp_start+1, gene->nSNP, gene->eSNP);
	fprintf(fp_VEGAS_result, "%g\t%d\t%d\t%g\n", gene->vegas, gene->hits.perm_vegas, gene->hits.hit_vegas, ((double)gene->hits.hit_vegas)/(gene->hits.perm_vegas));
      }
      if(VERBOSE)
	printf("print BF finished\n");
    }
  }
}
//get the additional parameters
void getEnv(){

  char* skipperm_s = getenv("SKIPPERM");
  if(skipperm_s != NULL && strcmp ( skipperm_s, "TRUE") == 0){
    SKIPPERM = true;
    printf("-Skip permutation tests\n");
  }else{
    SKIPPERM=false;
  }
  char* fullsearch_s = getenv("FULLSEARCH");
  if(fullsearch_s != NULL && strcmp ( fullsearch_s, "TRUE") == 0){
    FULLSEARCH = true;
    printf("-Search full model up to 3 SNPs\n");
  }else{
    FULLSEARCH=false;
  }

  DATASET = getenv("DATASET");
  if(DATASET == NULL){
    DATASET = "genes";
  }

  char* NPERM_s = getenv("NPERM");
  if(NPERM_s != NULL){
    sscanf(NPERM_s, "%d", &N_PERM);
  } else{
    N_PERM=1000000;
  }
  printf("-performing up to %d permutations\n", N_PERM);

  char* FLANK_s = getenv("FLANK");
  if(FLANK_s != NULL){
    sscanf(FLANK_s, "%d", &FLANK);
  } else{
    FLANK=20000;
  }
  printf("-searching %d flanking sequence\n", FLANK);


  char* INTERCEPT_s = getenv("INTERCEPT");
  if(INTERCEPT_s != NULL && strcmp(INTERCEPT_s, "FALSE") == 0){
    INTERCEPT=false;
    printf("-working in NO intercept mode\n");
  } else{
    INTERCEPT=true;
  }


  char* get_snp_bonf_pval_s = getenv("GET_MINSNP_P");
  if(get_snp_bonf_pval_s != NULL && strcmp (get_snp_bonf_pval_s, "FALSE") == 0){
    GET_SNP_PERM_PVAL = false;
    printf("-Skip minSNP-P\n");
  }else{
    GET_SNP_PERM_PVAL=true;
  }

  char* get_snp_pval_s = getenv("GET_MINSNP");
  if(get_snp_pval_s != NULL && strcmp ( get_snp_pval_s, "FALSE") == 0){
    GET_SNP_PVAL = false;
    printf("-Skip minSNP\n");
  }else{
    GET_SNP_PVAL=true;
  }
  char* get_gene_pval_s = getenv("GET_GWIS");
  if(get_gene_pval_s != NULL && strcmp ( get_gene_pval_s, "FALSE") == 0){
    GET_GENE_PVAL = false;
    printf("-Skip GWiS permutations\n");
  }else{
    GET_GENE_PVAL=true;
  }

  char* get_vegas_pval_s = getenv("GET_VEGAS_PVAL");
  if(get_vegas_pval_s != NULL && strcmp (get_vegas_pval_s, "TRUE") == 0){
    GET_VEGAS_PVAL = true;
    printf("-Implement VEGAS\n");
  }else{
    GET_VEGAS_PVAL=false;
  }

  char* get_bf_pval_s = getenv("GET_BIMBAM");
  if(get_bf_pval_s != NULL && strcmp (get_bf_pval_s, "FALSE") == 0){
    GET_BF_PVAL = false;
    printf("-Skip BIMBAM\n");
  }else{
    GET_BF_PVAL=true;
  }
}

// check file existance
bool file_exists(const char *filename){
  FILE *file;
  if ((file = fopen(filename, "r"))) //I'm sure, you meant for READING =)
    {
      fclose(file);
      return true;
    }
  return false;
}

//main function
int main(int nARG, char *ARGV[]){

  if(nARG!= 6){
    printf("./GWiS working_dir chr trait destination_dir random_seed\n");
    printf("--working_dir: dir having the trait, genotype files\n");
    printf("--chr: such as chr1, chr2 ...\n");
    printf("--trait: GWiS will look for \"trait.tfam\" in the working directory\n");
    printf("--destination_dir: destination for your output files\n");
    printf("--random_seed: An integer number anove 1\n");
    return -1;
  }
  getEnv();
  //where to get data
  char* working_dir = ARGV[1];
  //get chr number
  char* filename = ARGV[2];
  //get phenotype
  char* datset = ARGV[3];
  //where to write my files
  char* target_dir = ARGV[4];
  int shuffle_seed; 
  int chr = 1;
  //convert char to integer
  sscanf(ARGV[5], "%d", &shuffle_seed);
  sscanf(ARGV[2], "chr%d", &chr);
  //shuffle_seed=2;

  // input files
  char indiv_id_file[MAX_FILENAME_LEN] = "";
  char tfam_file[MAX_FILENAME_LEN] = "";
  char refseq_file[MAX_FILENAME_LEN]="";
  char mlinfo_file[MAX_FILENAME_LEN]="";
  char tped_file[MAX_FILENAME_LEN]="";
  char snp_info_file[MAX_FILENAME_LEN]="";

  // output files
  char allSNP_file[MAX_FILENAME_LEN]="";
  char snp_pval_file[MAX_FILENAME_LEN]="";
  char snp_bonf_pval_file[MAX_FILENAME_LEN]="";
  char bf_pval_file[MAX_FILENAME_LEN]="";
  char vegas_pval_file[MAX_FILENAME_LEN]="";
  char gene_SNP_file[MAX_FILENAME_LEN]="";
  char BIC_result_file[MAX_FILENAME_LEN]="";
  char BIC_perm_result_file[MAX_FILENAME_LEN]="";
  
  //input files
  getFilename(refseq_file, working_dir, DATASET, ".txt");
  getFilename(tfam_file, working_dir, datset, ".tfam");
  getFilename(indiv_id_file, working_dir, "individual", "_id.txt");
  getFilename(tped_file, working_dir, filename, ".tped");
  getFilename(snp_info_file, working_dir, filename, ".snp.info");
  getFilename(mlinfo_file, working_dir, filename, ".f1f2.mlinfo");

  //output files
  getFilename(allSNP_file, target_dir, filename, ".result.allSNP.txt");
  getFilename(snp_pval_file, target_dir, filename, ".result.minSNP.txt");
  getFilename(snp_bonf_pval_file, target_dir, filename, ".result.minSNP_P.txt");
  getFilename(bf_pval_file, target_dir, filename, ".result.BimBam.txt");
  getFilename(vegas_pval_file, target_dir, filename, ".result.vegas.txt");
  getFilename(gene_SNP_file, target_dir, filename, ".result.geneSNP.txt");
  getFilename(BIC_result_file, target_dir, filename, ".result.GWiS.model.txt");
  getFilename(BIC_perm_result_file, target_dir, filename,  ".result.GWiS.Pval.txt");

  //read genes from CCDS.txt or similar file
  GENE* gene = NULL;
  int nGene = readGene(refseq_file, &gene, chr);
  printf("-%d total genes loaded\n", nGene);
  if(nGene == 0){
    printf("-Time: %s", getTime());
    return EXIT_SUCCESS;
  }

  //open input files
  FILE *fp_tped =NULL;
  FILE *fp_snp_info = NULL;
  FILE *fp_mlinfo =NULL;

  //file with the genotype
  fp_tped = fopen(tped_file, "r");
  //file with info about SNPs
  fp_snp_info = fopen(snp_info_file, "r");
  //file with more info about SNPs
  if(file_exists(mlinfo_file))
    fp_mlinfo= fopen(mlinfo_file, "r");
  //a temporaral variable to read the file
  char sline_mlinfo[MAX_LINE_WIDTH];
  //skip the header line
  strcpy(sline_mlinfo, "");
  if(fp_mlinfo != NULL)
    fgets(sline_mlinfo, MAX_LINE_WIDTH, fp_mlinfo);


  //open output files
  FILE *fp_allSNP =  fopen(allSNP_file, "w");
  FILE *fp_gene_snp = fopen(gene_SNP_file, "w");
  
  FILE *fp_snp_pval = NULL;
  if(GET_SNP_PVAL){
    fp_snp_pval=  fopen(snp_pval_file, "w");
  } 
  FILE *fp_snp_perm_pval=NULL;
  if(GET_SNP_PERM_PVAL){
    fp_snp_perm_pval   =  fopen(snp_bonf_pval_file, "w");
  }

  FILE *fp_bf_pval =NULL;
  if(GET_BF_PVAL){
    fp_bf_pval =  fopen(bf_pval_file, "w");
  }

  FILE *fp_vegas_pval =NULL;
  if(GET_VEGAS_PVAL){
    fp_vegas_pval =  fopen(vegas_pval_file, "w");
  }

  FILE *fp_bic_result = fopen(BIC_result_file, "w");
  FILE *fp_bic_perm_result=NULL;
  if(GET_GENE_PVAL){
    fp_bic_perm_result = fopen(BIC_perm_result_file, "w");
  }


  int i;

  //state of the random number generator
  gsl_rng *r, *saved_r_state;
  //struct to save the phenotypes

  PHENOTYPE* phenotype;
  //get the phenotype
  
  phenotype = getPhenotype(tfam_file, indiv_id_file);
  
  scalePheno(phenotype);

  //can be init to any seed. 
  saved_r_state=initRand(1);

  r =initRand(shuffle_seed);
  printf("-Use random seed %d\n", shuffle_seed);

  //circular queue
  C_QUEUE * snp_queue =  cq_init(MAX_SNP_PER_GENE, sizeof(SNP));

  if(CQ_DEBUG)
    printf("Circular Q initialized, size = %lu\n", snp_queue->nsize);

  SNP* curSNP= NULL;

  int nSNP = 0;
  int curSNP_id = 0;
  int nReadyGene=0;
  int nGeneFinished = 0;
  int nGeneLeft = nGene;
  int nGeneNoSNP=0;
  GENE** readyGene = (GENE**) malloc(nGene*sizeof(GENE*));
  GENE* startGene = gene->next;
  double c0, c1, rss;

  gsl_rng_memcpy(saved_r_state, r);
  
  double * LD_dat = malloc(MAX_SNP_PER_GENE*MAX_SNP_PER_GENE*sizeof(double));
  double * Cov_dat = malloc(MAX_SNP_PER_GENE*MAX_SNP_PER_GENE*sizeof(double));
  int max_snp_per_gene=MAX_SNP_PER_GENE;
  gsl_matrix_view  geneLD;
  gsl_matrix_view  geneCov;

  if(VERBOSE)
    printf("start to print file headers\n");

  // print output file headers
  fprintf(fp_allSNP, "SNP_ID\tC0\tC1\tRSS\tf_stat\tlog10BF\tMAF\tR2\teSampleSize\tnGene\n");
  if(fp_snp_pval != NULL)
    fprintf(fp_snp_pval, "Chr\tGeneID\tName\tStart\tEnd\tLength\tSNPs\tTests\tSNP.name\tSNP.pos\tSNP.MAF\tSNP.R2\tchi2\tn.tot\tn.better\tpval\tisBest\n");

  if(fp_snp_perm_pval != NULL)
    fprintf(fp_snp_perm_pval, "Chr\tGeneID\tName\tStart\tEnd\tLength\tSNPs\tTests\tSNP.name\tSNP.pos\tSNP.MAF\tSNP.R2\tchi2\tn.tot\tn.better\tpval\tisBest\n");

  if(fp_bf_pval!=NULL){
    fprintf(fp_bf_pval, "Chr\tGeneID\tName\tStart\tEnd\tLength\tSNPs\tTests\tBF_sum\tn.tot\tn.better\tpval\n");
  }
  if(fp_vegas_pval!=NULL){
    fprintf(fp_vegas_pval, "Chr\tGeneID\tName\tStart\tEnd\tLength\tSNPs\tTests\tvegas\tn.tot\tn.better\tpval\n");
  }
  fprintf(fp_bic_result, "Chr\tGeneID\tName\tStart\tEnd\tLength\tSNPs\tTests\tSNP.name\tSNP.pos\tSNP.MAF\tSNP.R2\tK\tSSM\tBIC\tf.stat\tR2\n");

  if(fp_bic_perm_result != NULL){
    fprintf(fp_bic_perm_result, "Chr\tGeneID\tName\tStart\tEnd\tLength\tSNPs\tTests\tSNP.name\tSNP.pos\tSNP.MAF\tSNP.R2\tK\tSSM\tBIC\tf.stat\tR2\tn.stop\tn.better\tpval\n");
  }
  fprintf(fp_gene_snp, "SNP.name\tSNP.chr\tSNP.pos\tSNP.bp\tGeneID\tGene.name\tGene.start\tGene.end\tf.stat\tMAF\tR2\teSampleSize\n");

  if(VERBOSE){
    printf("done printing file headers\n");
  }

  printf("-Start to read SNPs\n");

  do{

    curSNP = readSNP(snp_queue,fp_tped, fp_snp_info, fp_mlinfo,  phenotype);

    if(curSNP != NULL){ 

      curSNP->geno_tss = tss(curSNP->geno,  phenotype->N_sample);

  
      curSNP->f_stat = runTest(curSNP->geno, phenotype->pheno_array->data, phenotype->N_sample, phenotype->tss_per_n, &c0, &c1, &rss);
      curSNP->sum_pheno_geno = covariance(curSNP->geno, phenotype->pheno_array->data, phenotype->N_sample) * phenotype->N_sample;
      curSNP->BF = getlogBF(phenotype->tss_per_n, curSNP->geno_tss/phenotype->N_sample, phenotype->N_sample, curSNP->sum_pheno_geno/phenotype->N_sample);

    }

    //save the last unfinished gene 
    nReadyGene = assignSNP2Gene(&startGene, curSNP,curSNP_id, readyGene, fp_gene_snp);    

    if(curSNP != NULL){ 
      fprintf(fp_allSNP, "%s\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%d\n", curSNP->name, c0, c1, rss/phenotype->N_sample, curSNP->f_stat,curSNP->BF,  curSNP->MAF, curSNP->R2, curSNP->eSampleSize, curSNP->nGene);
      nSNP++;
    }
    //work on genes that have all snps loaded
    if(nReadyGene > 0){
      for (i =0; i< nReadyGene; i++){
	if(readyGene[i]->nSNP == 0){
	  nGeneNoSNP++;
	  continue;
	}
	
	if(VERBOSE)
	  printf("working on %s, %d total snps\n", readyGene[i]->ccds, readyGene[i]->nSNP);
	nGeneFinished++;
	
	if(VERBOSE)
	  printf("calculating LD\n");

	if(max_snp_per_gene<readyGene[i]->nSNP){
	  free(LD_dat);
	  free(Cov_dat);
	  max_snp_per_gene=readyGene[i]->nSNP;
	  LD_dat = malloc(max_snp_per_gene*max_snp_per_gene*sizeof(double));
	  Cov_dat = malloc(max_snp_per_gene*max_snp_per_gene*sizeof(double));
	  if( LD_dat == NULL|| Cov_dat== NULL){
	    printf("Failed to rezied the LD or Cov matrix.  Possibly out of memory. Number of SNPs: %d\n", max_snp_per_gene);
	    abort();
	  }else
	    printf("Successfully resized the LD matrix to %d x %d\n", max_snp_per_gene, max_snp_per_gene);
	}
	geneLD=gsl_matrix_view_array (LD_dat, readyGene[i]->nSNP, readyGene[i]->nSNP);
	geneCov=gsl_matrix_view_array (Cov_dat, readyGene[i]->nSNP, readyGene[i]->nSNP);
	getGeneLD(readyGene[i], &geneLD.matrix, &geneCov.matrix, snp_queue, phenotype->N_sample);
	if(VERBOSE)
	  printf("calculating eSNP\n");
	readyGene[i]->eSNP = getGeneESNP_dan(readyGene[i]->LD, readyGene[i]->nSNP);
	//	getGeneESNP_matrix(readyGene[i]->LD, readyGene[i]->nSNP);
	//reduced to a function to print the eigen values of 
	//printCovEigen(readyGene[i]->Cov, readyGene[i]->nSNP, readyGene[i]->chr, readyGene[i]->name,fp_cor_eigen_vector );

	//start to do permutations
	if(VERBOSE)
	  printf("getPerm\n");

	printf("-Start to run permutations for %s (n=%d) at %s",readyGene[i]->name , readyGene[i]->nSNP, getTime());
	
	gsl_rng_memcpy(r, saved_r_state);
	getPerm(snp_queue, readyGene[i], phenotype, r, fp_bic_result, fp_bic_perm_result, fp_snp_pval, fp_snp_perm_pval, fp_bf_pval, fp_vegas_pval, NULL, NULL);
	printf("-End running permutations for %s (n=%d) at %s", readyGene[i]->name, readyGene[i]->nSNP ,getTime());
	cleanSNPinGene(readyGene[i], snp_queue);
	if(VERBOSE)
	  printf("gene finished\n");
	readyGene[i]->LD=NULL;
      }
    }
	  
    if(VERBOSE)
      printf("clean up\n");
      
    cleanSNPQ(snp_queue);

    nGeneLeft -= nReadyGene;

    if(curSNP!=NULL)
      printf("-%d snps scanned, %d genes finished, %d genes have no SNP,  %d gene remaining\n", nSNP, nGeneFinished,nGeneNoSNP,  nGeneLeft);
    
    if(curSNP!= NULL){
      if(curSNP->nGene>0){
	curSNP_id++;
      }else{
	if(curSNP->geno != NULL){
	  free(curSNP->geno);
	  curSNP->geno=NULL;
	}
	
	if(curSNP->r != NULL){
	  free(curSNP->r);
	  curSNP->r=NULL;
	}
	cq_shift(snp_queue);
	if(CQ_DEBUG)
	  printf("shifted, (start, end) = (%lu, %lu)\n", snp_queue->start, snp_queue->end);
      }
    }
  }while( nGeneLeft > 0 );
  printf("-Time: %s", getTime());
  
  //close input files;
  fclose(fp_tped);
  fclose(fp_snp_info);

  //close output files
  fclose(fp_allSNP);
  if(fp_snp_pval != NULL)
    fclose(fp_snp_pval);

  if(fp_snp_perm_pval != NULL)
    fclose(fp_snp_perm_pval);

  if(fp_bf_pval!=NULL)
    fclose(fp_bf_pval);

  if(fp_vegas_pval !=NULL)
    fclose(fp_vegas_pval);

  fclose(fp_gene_snp);
  fclose(fp_bic_result);
  if(fp_bic_perm_result != NULL)
    fclose(fp_bic_perm_result);
  //  fclose(fp_cor_eigen_vector);

  free(readyGene);
  gsl_rng_free (r);
  gsl_rng_free (saved_r_state);
  gsl_vector_free(phenotype->pheno_array);
  free(phenotype);
  //add code to free genes and SNPs

  return 0;
}
