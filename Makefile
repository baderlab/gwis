CC=gcc
CFLAGS=-g -Wall
LFLAGS= -lgsl -lgslcblas -lm 

all: genescan

genescan:  GWiS.o
		$(CC) -Wall -o GWiS  GWiS.o $(LFLAGS) 
GWiS.o: GWiS.c
	$(CC) $(CFLAGS) -c GWiS.c -o GWiS.o

