//    GWiS: a gene-based test of association
//    Copyright (C) 2011  Hailiang Huang, Pritam Chanda, Joel S. Bader and Dan E. Arking
//    contact: joel.bader@jhu.edu

//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.

//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.

//    You should have received a copy of the GNU General Public License along
//    with this program; if not, write to the Free Software Foundation, Inc.,
//    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


<COMPILE>
GNU Scientific Library (GSL) is required to compile and use GWiS (http://www.gnu.org/software/gsl/).
Type "make" to compile and link.  "GWiS" is the final execuable.

<DEMO>
type ./runSample.sh
GWiS will read the inputs from ./sample and write the outputs to ./result.

<USAGE>
./generateSim.sh 
Will generate a sample input for 20 indepedent SNPs and 8000
individuals.  The generated files will be at ./sample.
plink is required  (http://pngu.mgh.harvard.edu/~purcell/plink/)

./GWiS working_dir chr trait destination_dir random_seed
--working_dir: dir having the trait and the genotype files
--chr: such as chr1, chr2 ...
--trait: GWiS will look for "trait.tfam" in the working directory
--destination_dir: destination for your output files
--random_seed: random seed

<ADJUSTABLE PARAMETERS>
GWiS by default runs up to 1 million permutations and outputs results for GWiS, minSNP, minSNP-P and BIMBAM.  It is possible to change the these parameters by setting the GWiS environment variables.  An example to set the environment variable 'VAR' in the bash shell is  
	VAR=value_you_want_to_set
	export VAR

1) set the maximum number of permutations
	NPERM=number_of_max_permutations
	Example:
		NPERM=1000
		export NPERM
2) skip/restore GWiS permutations (GWiS model will still be estimated)
	GET_GWIS=FALSE/TRUE
3) skip/restore minSNP
	GET_MINSNP=FALSE/TRUE
4) skip/restore minSNP-P
	GET_MINSNP_P=FALSE/TRUE
5) skip/restore BIMBAM
	GET_BIMBAM=FALSE/TRUE

<OUTPUT>
chr?.result.allSNP.txt
	having the test statistics of each SNP in the study
chr?.result.geneSNP.txt
	mapping from SNPs to genes
chr?.result.minSNP.txt
	having the results from minSNP method	
chr?.result.minSNP_P.txt
	having the results from minSNP-P method	
chr?.result.BimBam.txt
	having the results from BimBam method	
chr?.result.GWiS.model.txt
	having the results from GWiS model selection
chr?.result.GWiS.Pval.txt
	having the results from GWiS permutations
