#!/bin/bash

nsample=8000
dir=./sample

if which plink
then
    echo ""
else
    echo "Plink has to be installed and the installation location has to be added to the PATH variable."
    exit 0
fi

#use plink to generate simulated trait and genotypes
plink --simulate-qt sample.sim --simulate-n $nsample  --recodeA --out sample

#transform the data into the tfam format
echo -e "#FID\tid\tDADID\tMOMID\tmale\ttrait" > $dir/trait.tfam
awk '/^per/{print NR-2"\t"NR-2"\t"0"\t"0"\t"0"\t"$6}' sample.raw >> $dir/trait.tfam

#generate the individual id
awk '/^[0-9]/{print $1}' $dir/trait.tfam > $dir/individual_id.txt

#generate the SNP positions

if [ -f $dir/chr1.snp.info ]
then 
    rm $dir/chr1.snp.info
fi
touch $dir/chr1.snp.info
for i in `seq 1 20`
do
    echo -e "rs$i\t1\t$i\t$i" >> $dir/chr1.snp.info
done

#generate the SNP allele frequencies
echo -e "SNP\tAl1\tAl2\tFreq\tMAF\tQuality\tRsq" > $dir/chr1.f1f2.mlinfo
awk '//{print "rs" NR "\t" 1 "\t" 1 "\t" $3 "\t" $3 "\t" 1 "\t"1}' sample.simfreq >> $dir/chr1.f1f2.mlinfo

#generate gene list
echo -e "GeneID:1\ttestGene\t1\t1\t20" > $dir/genes.txt

#transform the data into the tped format
awk '
{ 
    for (i=1; i<=NF; i++)  {
        a[NR,i] = $i
    }
}
NF>p { p = NF }
END {    
    for(j=7; j<=p; j++) {
        str=a[2,j]
        for(i=3; i<=NR; i++){
            str=str"\t"a[i,j];
        }
        print str
    }
}' sample.raw > $dir/chr1.tped

rm sample.raw
rm sample.simfreq
rm sample.log
